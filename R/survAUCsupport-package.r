#' survAUCsupport
#'
#' @name survAUCsupport
#' @docType package
#' @importFrom ipred getsurv
#' @importFrom coin wilcoxsign_test
#' @importFrom coin pvalue
NULL


predictFromCoefficients <- function(coeffs, X)
{
  P <- as.matrix(X[,names(coeffs)]) %*% coeffs
  names(P) <- rownames(X)
  P
}

##' Estimates S(t) for test set
##'
##' taken from Wiel et al., 2009
##' @param y response vector (survival/censoring times) for training samples
##' @param cens censoring status vector (0 = censored, 1 = observed) for training samples
##' @param lincomb vector of values of the linear predictor ('covariates times parameters') for training samples
##' @param lincombtest idem for test samples
##' @return S(t)
##' @author Wiel et al.
surv.func <- function(y,cens,lincomb,lincombtest){
  explincomb <- exp(lincomb)
  Riskset<- outer(y, y, "<=")
  breslows <- as.vector(cens / Riskset %*% explincomb)
  breslow <- as.vector(breslows %*% Riskset)
  yord <- order(y)
  ysort <- y[yord]
  H0sort <- c(0,breslow[yord])
  H0sort <- H0sort[-length(H0sort)]
  S0t <- exp(-H0sort)
  explincomb <- exp(lincombtest)
  St <- outer(S0t,explincomb,"^")
  return(St)
}

##' Integrated Brier residuals for test samples
##'
##' taken from Wiel et al., 2009
##' @param y response vector (survival/censoring times) for training samples
##' @param ytest idem for test samples
##' @param cens censoring status vector (0 = censored, 1 = observed) for training samples
##' @param censtest idem for test samples
##' @param survs output of \\code{\\link{surv.func}}
##' @return integrated brier scores for each of the test samples
##' @export
##' @author Wiel et al.
mybrier <- function(y,ytest,cens,censtest,survs) {
  hatcdist <- survfit(Surv(y, 1 - cens) ~ 1)
  csurv <- getsurv(hatcdist, ytest)
  csurv[csurv == 0] <- Inf
  csurvtrain <- getsurv(hatcdist, y)
  csurvtrain[csurvtrain == 0] <- Inf
  ytestord <- order(y)
  y <- y[ytestord]
  csurvtrain <- csurvtrain[ytestord]
  bscall <- c()
  if (length(y) > 1) {
    for (j in 1:length(y)) {
      help1 <- as.integer(ytest <= y[j] & censtest == 1)
      help2 <- as.integer(ytest > y[j])
      bsc <- (0 - survs[j, ])^2 * help1 * (1/csurv) +
          (1 - survs[j, ])^2 * help2 * (1/csurvtrain[j])
      bscall <- cbind(bscall,bsc)
    }
  }
  diffs <- c(y[1], y[2:length(y)] - y[1:(length(y) - 1)])
  RET <- bscall %*% diffs/max(y)
  return(RET)
}


##' Pvalues from Integrated Brier Residuals
##'
##' taken from Wiel et al., 2009
##' @param i numeric.  integer
##' @param altern character. direction of alternative hypothesis. Either "two.sided", "greater" (mu1 >= mu2) or "less".
##' @param residuallist a list of which the length equals the number of splits. Each list item should contain and m*2 matrix, which contains the residuals for the m test samples and two prediction procedures in a paired way.
##' @return results
##' @author Wiel et al., 2009
SummarySplits <- function(i,altern,residuallist){
  resids <- residuallist[[i]]
  average1 <- mean(resids[,1]);average2 <- mean(resids[,2])
  nobs <- nrow(resids)
  yx <- data.frame(y=c(resids[,1],resids[,2]),x=as.factor(c(rep(1,nobs),rep(0,nobs))),Blok = as.factor(rep(1:nobs,2)))
  distr <- ifelse(nobs <=20, "exact", "asymptotic")
  it <- wilcoxsign_test(y ~ x|Blok, data=yx, distribution=distr, alternative = altern)
  pcomp <- pvalue(it)
  results <- c(i,pcomp,average1,average2)

  return(results)
}


##' compare (by p-value) two brier score error curves
##'
##' Wrapped around the code from Wiel et al., 2009
##' @param object data.frame.  columns are different models, rows are time points, entries are brier scores
##' @param model1 character (or numeric).  refers to one column of object
##' @param model2 character (or numeric).  refers to one column of object
##' @return list with two combined pvalues, a histogram of pvalues and a data.frame with p values per split
##' @author Wiel et al., 2009 (wrapper by Andreas Leha)
compareBrierCurves <- function(object, model1, model2)
{
  allres <- lapply(object, '[', c(model1, model2))
  AllSummarySplits <- t(sapply(1:length(allres),
                               SummarySplits,
                               altern="greater",
                               residuallist=allres))
  colnames(AllSummarySplits) <-
      c("splitnr","p-value","averageProc1","averageProc2")

  ## results for all splits including conditional p-values
  ##AllSummarySplits

  ## generate histogram of conditional p-values
  Pvalues <- AllSummarySplits[,2]
  p_pvals_hist <-
      qplot(Pvalues,
            geom="histogram",
            breaks=c(0,0.1,0.25,0.5,0.75,0.9,1)) +
        scale_x_continuous(breaks=c(0,0.1,0.25,0.5,0.75,0.9,1)) +
            expand_limits(x=c(0,1)) +
                ylab("Frequency") +
                    theme_bw()
  ##hist(Pvalues,xaxt="n",breaks=c(0,0.1,0.25,0.5,0.75,0.9,1),freq=TRUE,ylab="Frequency",main="")
  ##axis(1,at=c(0,0.1,0.25,0.5,0.75,0.9,1),labels=c(0,0.1,0.25,0.5,0.75,0.9,1))

  ## p-tilde
  p_tilde <- median(Pvalues)

  ## p'
  p_prime <- pnorm(mean(qnorm(Pvalues)))

  list(SummarySplits=AllSummarySplits,
       histogram=p_pvals_hist,
       p_tilde=p_tilde,
       p_prime=p_prime)
}


##' make a data.frame with given vectors as columns.  Pad shorter vectors
##' with 'filling'.
##'
##' @title make a data.frame from vectors of (possibly) different length
##' @param ... vectors to be cbinded
##' @param filling this is used to fill shorter vectors. recycled if necessary
##' @return data.frame
##' @author Andreas Leha
cbind.fill <- function(..., filling="") {
  bindlist <- list(...)

  maxlength <- max(laply(bindlist, length))

  paddedlist <-
    llply(bindlist,
          function(vec, length) c(vec, rep(filling, maxlength-length(vec))),
          maxlength)

  retdf <- as.data.frame(paddedlist)
  names(retdf) <- names(bindlist)

  retdf
}



##' permutation to get a "NULL model"
##'
##' perform nperm permutations to get an estimate of a null model
##' @title calcNullSurvAUC
##' @param St Surv(...) object for the samples in the training set
##' @param Xt data.frame with the predictors (training set)
##' @param Xv data.frame with the predictors (validation set)
##' @param Sv Surv(...) object for the samples in the validation set
##' @param eval.times points in time when to evaluate the AUC curves
##' @param nperm number of sampling runs
##' @param modelling  character.  which function to use for the survival modelling.  Currently implemented are "coxph" and "CoxBoost" and "penalized".
##' @return list
##' \item{meas}{the estimated null model measure}
##' \item{times}{times at which the values of meas are estimated}
##' @author Andreas Leha
##' @export
calcNullSurvAUC <- function(St,
                            Xt,
                            Xv,
                            Sv,
                            eval.times,
                            nperm,
                            modelling="coxph") {
  if (modelling=="CoxBoost")
    if (!require("CoxBoost"))
      stop("package =CoxBoost= required for modelling with CoxBoost")
  if (modelling=="penalized")
    if (!require("penalized"))
      stop("package =penalized= required for modelling with penalized")

  available_measures <- c("brier", "uno", "hc", "sh", "cd")

  ## remove NA survival times (survAUC chokes on them)
  idx_NA <- is.na(Sv)

  allAUCs <-
    alply(1:nperm, 1, failwith(NULL, function(i)
        {
          Stperm <- St[sample(1:nrow(St), nrow(St)),]

          coxmo <- tryCatch(switch(modelling,
                                   coxph=coxph(reformulate(colnames(Xt), response="Stperm"),
                                     data=Xt,
                                     iter.max = 200),
                                   penalized={
                                     Xtrsf <- cbind(Xt, time=Stperm[,"time"], event=Stperm[,"status"])
                                     optlambda1 <- optL1(Surv(time, event) ~ ., data=Xtrsf,
                                        minlambda1=1, maxlambda1 = 10,
                                        trace=FALSE)
                                     penalized(Surv(time, event) ~ .,
                                               data=Xtrsf,
                                               lambda1 = optlambda1$lambda,
                                               trace=FALSE)},
                                   CoxBoost={
                                     cv.res <- cv.CoxBoost(time=Stperm[,"time"],
                                                           status=Stperm[,"status"],
                                                           x=as.matrix(Xt),
                                                           maxstepno=100,
                                                           K=min(10, nrow(Xt)),
                                                           type="verweij",
                                                           penalty=100)
                                     if (cv.res$optimal.step == 0)
                                       warning("CoxBoost suggests 0 steps")
                                     cbfit <- CoxBoost(time=Stperm[,"time"],
                                                       status=Stperm[,"status"],
                                                       x=as.matrix(Xt),
                                                       stepno=cv.res$optimal.step,
                                                       penalty=100)
                                     cbfit
                                   },
                                   stop(paste("unsupported modelling:", modelling))),
                            warning=function(w) {
                              stop("loosing one permutation: ", conditionMessage(w))
                            })


          lp <-
            switch(modelling,
                   penalized=predictFromCoefficients(coefficients(coxmo),
                     Xt),
                   predict(coxmo))

          while (length(lp) < dim(Xt)[1]) {
              print(paste(length(lp), dim(Xt)[1], sep=" < "))
              numNAs <- apply(Xt, 1, function(x) sum(is.na(x)))
              maxNAs <- which(numNAs == max(numNAs))[1]
              Xt <- Xt[-maxNAs,]
              Stperm <- Stperm[-maxNAs,]
              cm_formula <- reformulate(colnames(Xt),
                                        response="Stperm")
              coxmo <- switch(modelling,
                              coxph=coxph(cm_formula,
                                data=Xt,
                                iter.max = 200),
                              penalized={
                                Xtrsf <- cbind(Xt, time=Stperm[,"time"], event=Stperm[,"status"])
                                optlambda1 <- optL1(Surv(time, event) ~ ., data=Xtrsf,
                                        minlambda1=1, maxlambda1 = 10,
                                        trace=FALSE)
                                penalized(Surv(time,event) ~ .,
                                          data=Xtrsf,
                                          lambda1 = optlambda1$lambda,
                                          trace=FALSE)},
                              CoxBoost={
                                cv.res <- cv.CoxBoost(time=Stperm[,"time"],
                                                      status=Stperm[,"status"],
                                                      x=as.matrix(Xt),
                                                      maxstepno=100,
                                                      K=min(10, nrow(Xt)),
                                                      type="verweij",
                                                      penalty=100)
                                cbfit <- CoxBoost(time=Stperm[,"time"],
                                                  status=Stperm[,"status"],
                                                  x=as.matrix(Xt),
                                                  stepno=cv.res$optimal.step,
                                                  penalty=100)
                                cbfit
                              },
                              stop(paste("unsupported modelling:", modelling)))

              lp <-
                switch(modelling,
                       penalized=predictFromCoefficients(coefficients(coxmo),
                         Xt),
                       predict(coxmo))

            }
          lpnew <-
            switch(modelling,
                   penalized=t(predictFromCoefficients(coefficients(coxmo),
                     Xv)),
                   CoxBoost=predict(coxmo, as.matrix(Xv)),
                   predict(coxmo, Xv))

          iBS_per_sample <- mybrier(Stperm[,1],
                                    Sv[!(idx_NA | is.na(lpnew)),1],
                                    Stperm[,2],
                                    Sv[!(idx_NA | is.na(lpnew)),2],
                                    surv.func(Stperm[,1],Stperm[,2],as.vector(lp),lpnew[!(idx_NA | is.na(lpnew))]))

          Meas <- alply(available_measures, 1, function(meas)
                      {
                        calcSurvivalMeter(lp,
                                          lpnew[!(idx_NA | is.na(lpnew))],
                                          Sv[!(idx_NA | is.na(lpnew)),],
                                          Stperm,
                                          eval.times,
                                          meas=meas)
                      })
          Meas <- llply(Meas, '[[', 1)
          names(Meas) <- available_measures

          list(meas=Meas,
               lpnew={if (is.matrix(lpnew)) lpnew[1,] else lpnew},
               iBS=data.frame(perm=i, sample=rownames(iBS_per_sample), iBS=iBS_per_sample))
        }))

  allAUCs <- allAUCs[!unlist(lapply(allAUCs, is.null))]

  ttt <- ldply(allAUCs, function(x) data.frame(sample=names(x$lpnew), lpnew=x$lpnew))
  ttt <- ddply(ttt[,-1], .(sample), summarise, lpnew=mean(lpnew))
  all_lpnew <- ttt$lpnew
  names(all_lpnew) <- ttt$sample

  all_iBS <- unlist(daply(ldply(allAUCs, function(x) x$iBS), .(sample), summarize, iBS=mean(iBS)))
  ## re-order (just in case)
  all_iBS <- all_iBS[rownames(Xv)]

  allAUCs <-
    alply(available_measures, 1, function(meas)
        {
          lallAUCs <- ldply(allAUCs, function(x) t(x[[1]][[meas]]))

          if (all(dim(lallAUCs) == c(0,0))) {
            lallAUCs <- rep(NA, length(eval.times))
            ##lpnew <- rep(NA, nrow(Xv))
            ##names(lpnew) <- rownames(Xv)
          } else {
            lallAUCs <- apply(lallAUCs[,-1], 2, mean)
          }

          list(error=lallAUCs,
               times=eval.times,
               ierror=NA)
        })
  names(allAUCs) <- available_measures

  list(Meas=allAUCs,
       times=eval.times,
       lpnew=all_lpnew,
       iBS_per_sample=all_iBS)
}

##' find the number of time points near the end where the estimation in flat
##'
##' @title find_trimSurvAUCs
##' @param s the survAUC estimate
##' @param meas the measure (brier, uno, etc) to look at
##' @return number of time points at the end at which the estimation is flat
##' @author Andreas Leha
find_trimSurvAUCs <- function(s, meas) {
  ms <- s[[1]][[meas]]

  taucs <- ms[[1]]
  taucs <- rev(taucs)
  ntrim <- 0

  ## nothing to trim if so short already
  if (length(taucs) < 2) return(length(ms$times))

  if (all(is.na(taucs) | taucs == 0)) return(length(ms$times))

  while (is.na(taucs[ntrim+1])) ntrim <- ntrim+1
  nantrim <- ntrim
  lastauc <- taucs[ntrim+1]
  while (taucs[ntrim + 2] == lastauc) {
    ntrim <- ntrim + 1
    if ((ntrim+1)==length(taucs)) {
      return(nantrim)
    }
  }

  ntrim
}

##' remove the last part of the estimation "at the end"
##'
##' @title trimSurvAUCs
##' @param s the survAUC estimation
##' @param ntrim the number of time points to remove
##' @return the trimmed survAUC estimation
##' @author Andreas Leha
trimSurvAUCs <- function(s, ntrim) {
  for (i in 1:length(s[[1]])) {
    if (ntrim[[i]] != 0) {
      s[[1]][[i]][[1]] <- rev(rev(s[[1]][[i]][[1]])[-(1:ntrim[[i]])])
      s[[1]][[i]][["times"]] <- rev(rev(s[[1]][[i]][["times"]])[-(1:ntrim[[i]])])
    }
  }

  return(s)
}

calcSurvivalMeter <- function(lp, lpnew, Sv, St, eval.times, meas="brier", integrate_over_available=TRUE) {
  if (is.null(lp))
    return(list(auc=rep(NA, length(eval.times)),
                times=eval.times,
                iauc=NA))
  if (meas=="uno") {
    Meas <- AUC.uno(St, Sv, lpnew, eval.times)
  } else if (meas=="cd") {
    Meas <- AUC.cd(St, Sv, lp, lpnew, eval.times)
  } else if (meas=="sh") {
    Meas <- AUC.sh(St, Sv, lp, lpnew, eval.times)
  } else if (meas=="hc") {
    Meas <- AUC.uno(St, Sv, lpnew, eval.times)
  } else if (meas=="brier") {
    Meas <- predErr(St, Sv, lp, lpnew, eval.times)
  } else {
    stop("unsupported meas")
  }

  if (integrate_over_available) {
    trailingNaNs <- rle(is.nan(rev(Meas[[1]])) | is.infinite(rev(Meas[[1]])))
    if (trailingNaNs$values[1] == TRUE) {
      eval.times.shortened <- eval.times[1:(length(eval.times)-trailingNaNs$lengths[1])]
      Meas.int <- calcSurvivalMeter(lp, lpnew, Sv, St, eval.times.shortened, meas)
      Meas[[length(Meas)]] <- Meas.int[[length(Meas.int)]]
    }
  }

  Meas
}

##' Takes the union of all variables in the given predictor_sets and
##' performs univariate cox regressions.
##' The resulting p-values are used to sort the variables.
##' The 'worst' n are reported.
##'
##' @title sort the variables according to their significance and return the least 10
##' @param predictor_sets list of vectors.
##' @param n how many variables to report (maximal)
##' @param S Surv object
##' @param X data.frame holding values for the vars under consideration
##' @return character vector with the selected vars
##' @author Andreas Leha
getLeastInformative <- function(predictor_sets, n=10, S, X) {
  allused_predictors <- Reduce(union, predictor_sets)

  ## find the least informatve ones
  pva <- sapply(allused_predictors, function(pred)
              {
                coxmo <- coxph(reformulate(pred, response="S"), data=X)
                summary(coxmo)$coefficients[,'Pr(>|z|)']
              })
  least_info <- names(rev(sort(pva))[1:min(n, length(allused_predictors))])

  least_info
}


##' compare different sets of predictor variables for cox models
##'
##' @name comparePredictorSets
##' @param X data.frame containing (at least) the predictors
##' @param S Surv(...) object
##' @param predictor_sets list of colnames of X
##' @param idx_train row index of X and S marking the training set
##' @param idx_val row index of X and S marking the validation set
##' @param eval.times points in time at which to estimate meas
##' @param modelling character.  which function to use for the survival modelling.  Currently implemented are "coxph" and "CoxBoost" and "penalized".
##' @param nullmodel_perms number of permutations in the estimation for the nullmodel.  If NULL, no nullmodel is included.
##' @param nullmodel_predictor_set optional.  vector.  predictor set to use in the permutations to get the nullmodel.  If this is not provided the least informative 10 vars from the union of all other predictor sets are used.
##' @param nullmodel_modelling  character.  which function to use for the survival modelling in the nullmodelling.  Currently supported are "coxph" and "CoxBoost" and "penalized".  If NULL (the default), use modelling.
##' @return list
##' \item{measures}{list of survAUC returns for each element in predictor_sets}
##' \item{plot}{comparison plot}
##' @export
##' @author Andreas Leha
##' @examples
##' comparePredictorSets(pbc,
##'                      Surv(pbc$time, as.numeric(pbc$status>0)),
##'                      list(age=c("age"),
##'                           marker=c("age", "albumin", "bili")),
##'                      1:(nrow(pbc)/2),
##'                      ((nrow(pbc)/2)+1):nrow(pbc),
##'                      sort(pbc$time))
comparePredictorSets <- function(X,
                                 S,
                                 predictor_sets,
                                 idx_train,
                                 idx_val,
                                 eval.times,
                                 modelling="coxph",
                                 nullmodel_perms=NULL,
                                 nullmodel_predictor_set=NULL,
                                 nullmodel_modelling=NULL) {

  if (!require("survAUC"))
    stop("package =survAUC= required for this function")
  if (!require("plyr"))
    stop("package =plyr= required for this function")
  if (!require("ggplot2"))
    stop("package =ggplot2= required for this function")
  if ((modelling=="CoxBoost") ||
      ((!is.null(nullmodel_modelling)) &&
       (nullmodel_modelling=="CoxBoost")))
    if (!require("CoxBoost"))
      stop("package =CoxBoost= required for modelling with CoxBoost")
  if ((modelling=="penalized") ||
      ((!is.null(nullmodel_modelling)) &&
       (nullmodel_modelling=="penalized")))
    if (!require("penalized"))
      stop("package =penalized= required for modelling with penalized")


  available_measures <- c("brier", "uno", "hc", "sh", "cd")


  if (is.null(nullmodel_modelling))
    nullmodel_modelling <- modelling

  idx_S_missing <- is.na(S)
  if (sum(idx_S_missing) > 0) {
    message(paste("removing",
                  sum(idx_S_missing),
                  "samples from the analysis due to missing survival information"))
    S <- S[!idx_S_missing,]
    X <- X[!idx_S_missing,]
  }


  ## internally use simple predictor names
  ## because modelling might fail on typical gene symbols
  predictor_names <-
    data.frame(original_names=colnames(X),
               simple_names=paste0("predictor",
                 sprintf(paste0("%0",nchar(as.character(ncol(X))),"d"),
                         1:ncol(X))))
  simple_predictor_sets <-
    llply(predictor_sets, function(pset)
        {
          as.character(predictor_names$simple_names[match(pset,
                                                          predictor_names$original_names)])
        })
  simpleX <- X
  colnames(simpleX) <- predictor_names$simple_names

  ## subset the data
  Xt <- simpleX[idx_train,]
  Xv <- simpleX[idx_val,]
  St <- S[idx_train,]
  Sv <- S[idx_val,]

  ## remove NA survival times (survAUC chokes on them)
  idx_NA <- is.na(Sv)



  ## eval the measure on each "model"
  measures <-
    alply(names(predictor_sets), 1,
          function(model) {
            lp <- NULL
            lpnew <- NULL

            cm_formula <- reformulate(simple_predictor_sets[[model]],
                                      response="St")

            coxmo <- switch(modelling,
                            coxph=coxph(cm_formula,
                              data=Xt),
                            penalized={
                              Xtrsf <- data.frame(Xt[,simple_predictor_sets[[model]]])
                              colnames(Xtrsf) <- simple_predictor_sets[[model]]
                              Xtrsf <- cbind(Xtrsf,
                                             time=St[,"time"],
                                             event=St[,"status"])
                              optlambda1 <- optL1(Surv(time, event) ~ ., data=Xtrsf,
                                        minlambda1=1, maxlambda1 = 10,
                                        trace=FALSE)
                              penalized(Surv(time, event) ~ ., data=Xtrsf,
                                        lambda1=optlambda1$lambda,
                                        trace=FALSE)
                            },
                            CoxBoost={
                              Xtcb <- data.frame(Xt[,simple_predictor_sets[[model]]])
                              names(Xtcb) <- simple_predictor_sets[[model]]
                              Xtcb <- as.matrix(Xtcb)
                              if (ncol(Xtcb) == 1) {
                                NULL
                              } else {
                                cv.res <- cv.CoxBoost(time=St[,"time"],
                                                      status=St[,"status"],
                                                      x=Xtcb,
                                                      maxstepno=100,
                                                      K=min(10, nrow(Xtcb)),
                                                      type="verweij",
                                                      penalty=100)
                                cbfit <- CoxBoost(time=St[,"time"],
                                                  status=St[,"status"],
                                                  x=Xtcb,
                                                  stepno=cv.res$optimal.step,
                                                  penalty=100)
                                cbfit
                              }
                            },
                            stop(paste("unsupported modelling:", modelling)))

            if (is.null(coxmo)) {
              lp <- NULL
              lpnew <- NULL
              iBS_per_sample <- NULL
              Meas <- calcSurvivalMeter(lp,
                                        lpnew[!(idx_NA | is.na(lpnew))],
                                        Sv[!(idx_NA | is.na(lpnew)),],
                                        St,
                                        eval.times,
                                        meas=meas)
              selected_features <- NULL
            } else {
              lp <-
                switch(modelling,
                       penalized=predictFromCoefficients(coefficients(coxmo),
                         Xt),
                       predict(coxmo))

              while (length(lp) < dim(Xt)[1]) {
                print(paste(length(lp), dim(Xt)[1], sep=" < "))
                numNAs <- apply(Xt[,simple_predictor_sets[[model]]],
                                1,
                                function(x) sum(is.na(x)))
                maxNAs <- which(numNAs == max(numNAs))[1]
                Xt <- Xt[-maxNAs,]
                St <- St[-maxNAs,]
                cm_formula <- reformulate(simple_predictor_sets[[model]],
                                          response="St")
                coxmo <-
                  switch(modelling,
                         coxph=coxph(cm_formula,
                           data=Xt),
                         penalized={
                           Xtrsf <- data.frame(Xt[,simple_predictor_sets[[model]]])
                           colnames(Xtrsf) <- simple_predictor_sets[[model]]
                           Xtrsf <- cbind(Xtrsf, time=St[,"time"], event=St[,"status"])
                           optlambda1 <- optL1(Surv(time, event) ~ ., data=Xtrsf,
                                        minlambda1=1, maxlambda1 = 10,
                                        trace=FALSE)
                           penalized(Surv(time, event) ~ .,
                                     data=Xtrsf,
                                     lambda1=optlambda1$lambda,
                                     trace=FALSE)},
                         CoxBoost={
                           cv.res <- cv.CoxBoost(time=St[,"time"],
                                                 status=St[,"status"],
                                                 x=as.matrix(Xt[,simple_predictor_sets[[model]]]),
                                                 maxstepno=100,
                                                 K=min(10, nrow(Xt)),
                                                 type="verweij",
                                                 penalty=100)
                           cbfit <- CoxBoost(time=St[,"time"],
                                             status=St[,"status"],
                                             x=as.matrix(Xt[,simple_predictor_sets[[model]]]),
                                             stepno=cv.res$optimal.step,
                                             penalty=100)
                           cbfit
                         },
                         stop(paste("unsupported modelling:", modelling)))

                lp <-
                  switch(modelling,
                         penalized=predictFromCoefficients(coefficients(coxmo),
                           Xt),
                         predict(coxmo))

              }

              ## the following two lines are needed in case length(predictor_sets[[model]])==1
              tmpXv <- data.frame(Xv[,simple_predictor_sets[[model]]])
              colnames(tmpXv) <- simple_predictor_sets[[model]]
              lpnew <-
                switch(modelling,
                       penalized=t(predictFromCoefficients(coefficients(coxmo),
                         tmpXv)),
                       CoxBoost=predict(coxmo, as.matrix(tmpXv)),
                       predict(coxmo, tmpXv))
              lpnewnames <- if (!is.null(names(lpnew))) {
                names(lpnew)
              } else if (!is.null(colnames(lpnew))) {
                colnames(lpnew)
              } else {
                rownames(tmpXv)
              }
              lpnew <- as.vector(lpnew)
              names(lpnew) <- lpnewnames

              iBS_per_sample <- mybrier(St[,1],
                                        Sv[,1],
                                        St[,2],
                                        Sv[,2],
                                        surv.func(St[,1],St[,2],as.vector(lp),lpnew))

              Meas <- alply(available_measures, 1, function(meas)
                          {
                            calcSurvivalMeter(lp,
                                              lpnew[!(idx_NA | is.na(lpnew))],
                                              Sv[!(idx_NA | is.na(lpnew)),],
                                              St,
                                              eval.times,
                                              meas=meas)
                          })
              names(Meas) <- available_measures


              selected_features <-
                switch(modelling,
                       coxph=predictor_sets[[model]],
                       penalized=predictor_names$original_names[match(names(coefficients(coxmo)), predictor_names$simple_names)],
                       CoxBoost=predictor_names$original_names[match(coxmo$xnames[which(coxmo$coefficients[coxmo$stepno+1, ] != 0)], predictor_names$simple_names)],
                       stop(paste("unsupported modelling:", modelling)))
            }

            c(Meas=list(Meas), list(lp=lp, lpnew=lpnew, selected_features=selected_features), iBS_per_sample=list(iBS_per_sample))
          })
  names(measures) <- names(predictor_sets)

  feature_sets <- do.call(cbind.fill, llply(measures, function(x) x$selected_features))

  if (!is.null(nullmodel_perms)) {
    if (is.null(nullmodel_predictor_set)) {
      use_nullmodel_predictor_set <- getLeastInformative(simple_predictor_sets, 10, S, simpleX)
    } else {
      use_nullmodel_predictor_set <- nullmodel_predictor_set
    }

    nullMeas <- calcNullSurvAUC(St=St,
                                Xt=Xt[,use_nullmodel_predictor_set],
                                Xv=Xv[,use_nullmodel_predictor_set],
                                Sv=Sv,
                                eval.times=eval.times,
                                nperm=nullmodel_perms,
                                modelling=nullmodel_modelling)
    measures$nullmodel <- nullMeas
  }

  ntrims <- alply(available_measures, 1, function(meas)
                {
                  laply(measures, find_trimSurvAUCs, meas)
                })
  ntrims <- lapply(ntrims, min, na.rm=TRUE)
  measures <- llply(measures, trimSurvAUCs, ntrims)

  plot_measures <-
    alply(available_measures, 1, function(meas)
        {
          adply(names(measures), 1,
                function(model) {
                  data.frame(model=model,
                             time=measures[[model]][[1]][[meas]][["times"]],
                             measure=measures[[model]][[1]][[meas]][[1]])
                })
        })
  names(plot_measures) <- available_measures

  plots <-
    llply(available_measures, function(meas)
        {
          ggplot(plot_measures[[meas]],
                 aes_string(x="time", y="measure", colour="model")) +
                   geom_path() +
                     theme_bw() +
                       ylab(meas) +
                         ylim(range(c(0, plot_measures[[meas]]$measure[!is.infinite(plot_measures[[meas]]$measure)]), na.rm=TRUE))
        })
  names(plots) <- available_measures

  list(measures=measures,
       feature_sets=feature_sets,
       plots=plots)
}



##' Similar to \\code{\\link{comparePredictorSets}}, this function
##' compares sets of variables based on their predictive power wrt
##' a time-to-event endpoint.  Unlike \\code{\\link{comparePredictorSets}},
##' this function performs a cross-validation.
##'
##' @title calculating survival prediction meters in a cross validation
##' @param X data.frame containing (at least) the preditors
##' @param S survival object
##' @param predictor_sets named list.  each element is a vector containing a subset of colnames(X)
##' @param nfolds int.  how many folds to do
##' @param eval.times numeric vector.  points in time at which to calculate the meter
##' @param modelling character.  which function to use for the survival modelling.  Currently implemented are "coxph" and "CoxBoost" and "penalized".
##' @param use_stable_predictor_sets boolean.  if TRUE use the features, that have been selected in at least 50% of the ninnerfolds folds
##' @param inner_modelling character.  which function to use for the survival modelling in the inner cross-validation for the stable feature seleciton
##' @param ninnerfolds integer.  how many folds to use in the inner cross validation.
##' @param nullmodel_perms int or NULL.  if NULL no nullmodel is included.  Otherwise the number of permutations to use to 'create' a nullmodel
##' @param nullmodel_predictor_set optional.  vector.  predictor set to use in the permutations to get the nullmodel.  If this is not provided the least informative 10 vars from the union of all other predictor sets are used.
##' @param nullmodel_modelling character.  which function to use for the survival modelling in the nullmodelling.  Currently supported are "coxph" and "CoxBoost" and "penalized".  If NULL (the default), use modelling.
##' @param draw_fold_lines boolean.  if TRUE, the meter is (additionally) calculated in each fold and is plotted in the background of the plot.
##' @param colours which colours to use in for the provided predictor sets
##' @param cvprogress display the progress (in cv folds).  possible values see \code{plyr::adply}
##' @param cvparallel boolean.  if TRUE, do the cv folds in parallel.  needs an initialized backend.  See \code{plyr::adply} for details
##' @param createRNGstreams boolean.  should new RNG streams be created per fold (recommended!!!! for normal use)
##' @return list.
##' \item{measures}{the numbers}
##' \item{plot}{a ggplot2 plot of the numbers}
##' @author Andreas Leha
##' @export
##' @examples
##' cvcomp <-
##'   cv_comparePredictorSets(X=pbc,
##'                           S=Surv(pbc$time, as.numeric(pbc$status>0)),
##'                           predictor_sets=list(age=c("age"),
##'                                               marker=c("age", "albumin", "bili")),
##'                           nfolds=10,
##'                           eval.times=sort(pbc$time),
##'                           nullmodel_perms=10,
##'                           draw_fold_lines=TRUE)
##' cvcomp$plot
cv_comparePredictorSets <- function(X,
                                    S,
                                    predictor_sets,
                                    nfolds,
                                    eval.times,
                                    modelling="coxph",
                                    use_stable_predictor_sets=FALSE,
                                    inner_modelling="penalized",
                                    ninnerfolds=10,
                                    nullmodel_perms=NULL,
                                    nullmodel_predictor_set=NULL,
                                    nullmodel_modelling=NULL,
                                    draw_fold_lines=FALSE,
                                    colours=NULL,
                                    cvprogress="none",
                                    cvparallel=FALSE,
                                    createRNGstreams=TRUE) {
  if ((modelling=="CoxBoost") ||
      ((!is.null(nullmodel_modelling)) &&
       (nullmodel_modelling=="CoxBoost")))
    if (!require("CoxBoost"))
      stop("package =CoxBoost= required for modelling with CoxBoost")
  if ((modelling=="penalized") ||
      ((!is.null(nullmodel_modelling)) &&
       (nullmodel_modelling=="penalized")))
    if (!require("penalized"))
      stop("package =penalized= required for modelling with penalized")
  if (!require("rlecuyer"))
    stop("package =rlecuyer= required for (parallel) and reproducible random numbers")

  available_measures <- c("brier", "uno", "hc", "sh", "cd")

  if (is.null(nullmodel_modelling))
    nullmodel_modelling <- modelling

  idx_S_missing <- is.na(S)
  if (sum(idx_S_missing) > 0) {
    message(paste("removing",
                  sum(idx_S_missing),
                  "samples from the analysis due to missing survival information"))
    S <- S[!idx_S_missing,]
    X <- X[!idx_S_missing,]
  }

  if (createRNGstreams) {
    ## prepare the parallel RNG
    Lecuyerstreamnames <- paste(paste(sample(LETTERS, 10), collapse = ""),
                                sprintf("%05d", 1:nfolds),
                                sep="_")
    Lecuyerseeds <- sample(1000000, 6, replace=FALSE)
    ## restore old RNG state if it is set
    if (exists(".Random.seed")) {
      oldseed <- get(".Random.seed", .GlobalEnv)
      on.exit(assign(".Random.seed", oldseed, .GlobalEnv))
    }
    .lec.SetPackageSeed(Lecuyerseeds)
    .lec.CreateStream(Lecuyerstreamnames)
  }

  n <- nrow(S)
  pred_results <-
    alply(1:nfolds, 1, function(fold)
        {
          if (createRNGstreams) {
            .lec.CurrentStream(Lecuyerstreamnames[fold])
          }

          idx_val <- seq(fold, n, by=nfolds)
          idx_train <- (1:n)[-idx_val]

          if (use_stable_predictor_sets) {
            stable_predictor_sets <-
              selectStableFeatureSet(X=X[idx_train,],
                                     S=S[idx_train,],
                                     predictor_sets = predictor_sets,
                                     nfolds = ninnerfolds,
                                     modelling=inner_modelling,
                                     eval.times=eval.times,
                                     colours=colours,
                                     cvprogress="none",
                                     cvparallel=FALSE,
                                     createRNGstreams=FALSE)
            stable_predictor_sets <- stable_predictor_sets$stable_feature_sets
            stable_predictor_sets <- llply(stable_predictor_sets, as.character)
            print(stable_predictor_sets)
          }

          ttt <- comparePredictorSets(X=X,
                                      S=S,
                                      predictor_sets = if (use_stable_predictor_sets) stable_predictor_sets else predictor_sets,
                                      idx_train = idx_train,
                                      idx_val = idx_val,
                                      eval.times = eval.times,
                                      modelling = modelling,
                                      nullmodel_perms = nullmodel_perms,
                                      nullmodel_predictor_set = nullmodel_predictor_set,
                                      nullmodel_modelling = nullmodel_modelling)

          lpnews <-
            adply(names(predictor_sets), 1, function(pset)
                {
                  if (length(ttt$measures[[pset]]$selected_features) == 0) {
                    data.frame(predictor_set=pset,
                               sample=rownames(X)[idx_val],
                               lpnew=NA)
                    } else {
                      data.frame(predictor_set=pset,
                                 sample={if (is.null(names(ttt$measures[[pset]]$lpnew)))
                                           colnames(ttt$measures[[pset]]$lpnew) else names(ttt$measures[[pset]]$lpnew)},
                                 lpnew={if (is.matrix(ttt$measures[[pset]]$lpnew)) ttt$measures[[pset]]$lpnew[1,] else ttt$measures[[pset]]$lpnew})
                    }
                })[,-1]

          meass <-
            alply(available_measures, 1, function(meas)
                {
                  adply(names(predictor_sets), 1, function(pset)
                      {
                        data.frame(predictor_set=pset,
                                   fold=fold,
                                   meas=meas,
                                   times=ttt$measures[[pset]][[1]][[meas]]$times,
                                   measure=ttt$measures[[pset]][[1]][[meas]][[1]])
                      })[,-1]
                })
          names(meass) <- available_measures

          if (!is.null(nullmodel_perms)) {
            lpnew_null <-
              data.frame(predictor_set="nullmodel",
                         sample=names(ttt$measures$nullmodel$lpnew),
                         lpnew=ttt$measures$nullmodel$lpnew)
            lpnews <- rbind(lpnews,lpnew_null)
            meas_null <-
              alply(available_measures, 1, function(meas)
                  {
                    data.frame(predictor_set="nullmodel",
                               fold=fold,
                               meas=meas,
                               times=ttt$measures$nullmodel[[1]][[meas]]$times,
                               measure=ttt$measures$nullmodel[[1]][[meas]][[1]])
                  })
            names(meas_null) <- available_measures
            meass <- alply(names(meass), 1, function(meas) rbind(meass[[meas]], meas_null[[meas]]))
            names(meass) <- available_measures
          }

          if (createRNGstreams) {
            .lec.CurrentStreamEnd()
          }

          list(measlpnew=lapply(meass, rbind.fill, lpnews),
               cvinnerres=ttt,
               samples_train=rownames(X)[idx_train],
               samples_val=rownames(X)[idx_val])
        },
          .progress=cvprogress,
          .parallel=cvparallel)

  if (createRNGstreams) {
    .lec.DeleteStream(Lecuyerstreamnames)
  }

  plots_measures <-
    alply(available_measures, 1, function(meas)
      {
        lpnew <- ldply(pred_results, function(x) x$measlpnew[[meas]])
        lpnew$fold <- factor(lpnew$fold, ordered=TRUE)

        plot_measures <- ddply(lpnew[is.na(lpnew$sample),],
                               .(predictor_set, times),
                               summarise,
                               Measure=mean(measure, na.rm=TRUE))
        list(lpnew=lpnew, plot_measures=plot_measures)
      })
  names(plots_measures) <- available_measures

  plots <-
    alply(names(plots_measures), 1, function(meas)
        {
          plot_measures <- plots_measures[[meas]]

          plot <-
            ggplot(plot_measures$plot_measures,
                   aes_string(x="times", y="Measure", colour="predictor_set")) +
              geom_path() +
                theme_bw() +
                  ylab(switch(meas,
                              brier="Prediction Error (Brier Score)",
                              sh="dynamic AUC (Song and Zhou estimator)",
                              cd="dynamic AUC (Chambless and Diao estimator)",
                              uno="dynamic AUC (Uno estimator)",
                              hc="dynamic AUC (Hung and Chiang estimator)")) +
                                ylim(range(c(0, plot_measures$plot_measures$Measure[!is.infinite(plot_measures$plot_measures$Measure)]), na.rm=TRUE)) +
                                  xlab("Time")

          if (draw_fold_lines)
            plot <-
              plot + geom_path(aes(x=times,
                                   y=measure,
                                   colour=predictor_set,
                                   group=fold:predictor_set),
                               alpha=0.1,
                               data=plot_measures$lpnew[!is.na(plot_measures$lpnew$fold),])

          if (!is.null(colours))
            plot <- plot + scale_colour_manual(values=colours)

          plot
        })
  names(plots) <- available_measures

  plots_dir <-
    llply(plots, function(plot)
      {
        if (!require(directlabels)) {
          print("Package =directlables= not found -- skipping directly labelled plot")
          plot_dir <- NULL
        } else {
          plot_dir <-
            direct.label(plot +
                         xlim(c(    min(plot$data$times),
                                1.2*max(plot$data$times))),
                         method=list(last.points, hjust=-0.05, cex=0.7))
        }

        plot_dir
      })
  names(plots_dir) <- available_measures


  numsel <-
    ldply(pred_results, function(x)
        {
          ldply(x$cvinnerres$measures,
                function(y) data.frame(numFeatures=length(y$selected_features)))
        })
  colnames(numsel)[1:2] <- c("Fold", "Model")

  plot_number_features <-
    ggplot(numsel, aes_string(x="Fold", y="numFeatures", fill="Model")) +
      geom_bar(stat="identity") +
        facet_wrap(~Model, ncol=1) +
          ylab("Number of Selected Features") +
            theme_bw()
  if (!is.null(colours)) {
    plot_number_features <- plot_number_features + scale_fill_manual(values=colours, guide=FALSE)
  } else {
    plot_number_features <- plot_number_features + scale_fill_discrete(guide=FALSE)
  }

  thesel <-
    ldply(pred_results, function(x)
        {
          ldply(x$cvinnerres$measures,
                function(y) data.frame(sel=y$selected_features))
        })
  if (!0 %in% dim(thesel)) {
    colnames(thesel)[1:2] <- c("Fold", "Model")

    ## reorder for nicer plotting
    norder <- rev(do.call(order,
                          c(list(overall=table(thesel$sel)),
                            alply(table(thesel$sel, thesel$Model), 2, function(x) x))))

    thesel$sel <-
      factor(thesel$sel,
             levels=names(table(thesel$sel))[norder])
  }

  plot_inclusion_num <-
    ggplot(thesel, aes_string(x="sel", fill="Model")) +
      geom_bar() +
        facet_wrap(~Model, ncol=1) +
          xlab("Feature") +
            ylab("Number of Folds in Which Selected") +
              theme_bw() +
                scale_y_continuous(breaks=0:nfolds, limits=c(0,nfolds)) +
                  theme(axis.text.x=element_text(angle=45, hjust=1, vjust=1))
  if (!is.null(colours)) {
    plot_inclusion_num <- plot_inclusion_num + scale_fill_manual(values=colours, guide=FALSE)
  } else {
    plot_inclusion_num <- plot_inclusion_num + scale_fill_discrete(guide=FALSE)
  }

  if (0 %in% dim(thesel)) {
    seltable <- NULL
    seldf <- NULL
    plot_inclusion_freq <- NULL
  } else {
    seltable <- table(thesel$sel, thesel$Model)
    seltablerownames <- rownames(seltable)
    seltablecolnames <- colnames(seltable)
    seltable <- seltable[rev(do.call(order, c(list(overall=table(thesel$sel)), alply(table(thesel$sel, thesel$Model), 2, function(x) x)))),]
    seltable <- as.matrix(seltable)
    rownames(seltable) <- seltablerownames
    colnames(seltable) <- seltablecolnames
    seltable <- 100*(seltable/nfolds)

    if (length(predictor_sets) > 1) {
      seldf <- melt.array(seltable, varnames = c("Feature", "Model"))
      colnames(seldf)[3] <- "InclusionFrequency"
    } else {
      seldf <- data.frame(Model=names(predictor_sets),
                          Feature=(if (is.null(names(seltable))) rownames(seltable) else names(seltable)),
                          InclusionFrequency=(if (dim(seltable)[2] == 1) seltable[,1] else seltable))
    }
    rownames(seldf) <- NULL

    ## reorder
    seldf$Feature <-
      factor(seldf$Feature,
             levels=levels(thesel$sel))

    plot_inclusion_freq <-
      ggplot(seldf[seldf$InclusionFrequency>0,],
           aes_string(x="Feature", y="InclusionFrequency", fill="Model")) +
             geom_bar(stat="identity") +
               ylab("Inclusion Frequency [%]") +
                 theme_bw() +
                   facet_wrap(~Model, ncol=1) +
                     theme(axis.text.x=element_text(angle=45,
                             hjust=1, vjust=1))
    if (!is.null(colours)) {
      plot_inclusion_freq <- plot_inclusion_freq + scale_fill_manual(values=colours, guide=FALSE)
    } else {
      plot_inclusion_freq <- plot_inclusion_freq + scale_fill_discrete(guide=FALSE)
    }
  }

  ## calculate some p values comparing the brier curves
  ## see Wiel et al., 2009
  available_predictor_sets <- names(predictor_sets)
  if (!is.null(nullmodel_perms)) available_predictor_sets <- c(available_predictor_sets, "nullmodel")
  brier_residuals <-
      alply(as.character(1:nfolds), 1, function(fold) {
        fold_res <-
            alply(available_predictor_sets, 1, function(predictor_set) {
              pred_results[[fold]]$cvinnerres$measures[[predictor_set]]$iBS_per_sample
            })
        names(fold_res) <- available_predictor_sets

        as.data.frame(fold_res)
      })
  names(brier_residuals) <- 1:nfolds

  comparisons <- expand.grid(available_predictor_sets, available_predictor_sets)
  comparisons <- comparisons[comparisons[,1] != comparisons[,2],]
  pairwise_comparisons <- apply(comparisons, 1, function(x) failwith(NULL, compareBrierCurves)(brier_residuals, x[1], x[2]))
  names(pairwise_comparisons) <- sapply(names(pairwise_comparisons), function(nn) paste(as.character(unlist(comparisons[nn,])), collapse="_greater_"))

  plots <- list(measure=plots,
                measure_dir=plots_dir,
                number_features=plot_number_features,
                inclusion_num=plot_inclusion_num,
                inclusion_freq=plot_inclusion_freq)

  list(## pass through of the input data
       X=X,
       S=S,
       ## pass through of some important parameters
       predictor_sets=predictor_sets,
       nfolds=nfolds,
       eval.times=eval.times,
       modelling=modelling,
       inner_modelling=inner_modelling,
       ninnerfolds=ninnerfolds,
       ## the results
       cv_results=pred_results,
       measures=llply(plots_measures, '[[', "plot_measures"),
       number_features=numsel,
       selected_features=thesel,
       inclusion_freq=seldf,
       plots=plots,
       pairwise_comparisons=pairwise_comparisons)
}

##' report the features selected stably in a penalized survival model
##'
##' Internally this function performs a cross-validation and reports
##' the features, that get selected in at least incfreq folds.
##' @title report the features selected stably in a penalized survival model
##' @param X data.frame with the the predictors as columns and the samples as rows
##' @param S Surv object.  nrow(S) == nrow(X) is required
##' @param predictor_sets named list. each element has to be a vector of column names of X
##' @param nfolds integer
##' @param incfreq numeric.  In how many folds the stable features have to be present.  To be specified in percent.  Defaults to 50.
##' @param modelling character.  the survival model function to use.  Supported are currently 'coxph', 'CoxBoost' and 'penalized' but 'coxph' does not do any feature selection, so is not meaningful here
##' @param eval.times numeric vector.
##' @param colours vector
##' @param cvprogress character.  display the progress?
##' @param cvparallel boolean.
##' @param createRNGstreams boolean.  create new RNG stream for folds?
##' @export
##' @return named list of stable feature sets.  one item per item in predictor_sets
##' @author Andreas Leha
selectStableFeatureSet <- function(X,
                                   S,
                                   predictor_sets,
                                   nfolds,
                                   incfreq=50,
                                   modelling="penalized",
                                   eval.times,
                                   colours,
                                   cvprogress="none",
                                   cvparallel=FALSE,
                                   createRNGstreams=TRUE) {

  inner_cv_results <-
    cv_comparePredictorSets(X=X,
                            S=S,
                            predictor_sets=predictor_sets,
                            nfolds=nfolds,
                            modelling=modelling,
                            eval.times=eval.times,
                            nullmodel_perms=NULL,
                            colours=colours,
                            cvprogress=cvprogress,
                            cvparallel=cvparallel,
                            createRNGstreams=FALSE)

  stable_feature_sets <-
    dlply(inner_cv_results$inclusion_freq, .(Model), function(x)
          x$Feature[x$InclusionFrequency >= incfreq])

  list(cv_results=inner_cv_results,
       stable_feature_sets=stable_feature_sets)
}
