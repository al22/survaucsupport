# survAUCsupport

Compare different sets of predictors with respect to their survivalAUC.

Includes permutation function to add a null-model.

## Installation

Install using the `devtools` package

    library("devtools")
    install_git("https://gitlab.com/al22/survaucsupport.git")